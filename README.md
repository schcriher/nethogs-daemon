# Nethogs Daemon
This daemon saves in a file the state of nethogs


## Introducción

[Nethogs](https://github.com/raboof/nethogs.git) es una herramienta que muestra el
consumo de ancho de banda por cada proceso, a diferencia que la mayoría de herramientas
de este tipo que muestran el consumo de ancho de banda por protocolo o subred.

Esta herramienta, `nethogs-daemon`, sirve para poner esa información en un archivo,
el cual puede ser consultado por un software externo como [conky](conky.sourceforge.net).


## Instalación

### Método A
Puede bajar el código fuente de nethogs compilarlo e instalarlo, hay que tener las
librerias libpcap, libpcap-dev y libncurses-dev instaladas, con los siguientes comandos:

    git clone https://github.com/raboof/nethogs.git
    cd nethogs
    make libnethogs
    make install_lib

Luego descargar (de este repositorio) el demonio `nethogs-daemon.py` y ejecutarlo
como root. Al hacerlo generará un archivo de salida que es el que incluirá en conky.

### Método B
Puede usar el `installer` que está en este repositorio, ejecutarlo como root, para
que descargue, compile e instale tanto la librería de nethogs como el demonio.

### Método C
Puede usar el archivo `builder` para generar un archivo `.deb` el cual puede instalar
y tener disponible la librería y el demonio en su sistema.

### Advertencia
El método B y C escriben sobre los mismos archivos, no use los dos juntos.</div>


## Uso

Al ejecutar el demonio puede configurar las siguientes opciones (parámetros):

| Opción | Descripción |
| :----: | :---------- |
| `-l`   | Localización de la librería `libnethogs.so`, la dirección absoluta o relativa al demonio, o solo el nombre exacto del archivo y Python la buscará entre las librerías del sistema. |
| `-o`   | Localización del archivo de salida, por ejemplo para ser incluido en conky. |
| `-t`   | Plantilla del archivo de salida, los comandos en conky usan llaves, estas deben introducirse duplicadas, ejemplo `${{goto 0}}`, para que Python no las interprete como variables. Los datos disponibles en el demonio se acceden con la variable `o`, ejemplo `{o.recv_kbs}`; puede usar el [formateo de cadenas de Python](https://docs.python.org/3/library/string.html#format-string-syntax), ejemplo `{o.recv_kbs:.2f}` para mostrar dos decimales. Abajo se listan las variables disponibles. |
| `-e`   | Lista de comandos a excluir por usuario, con el formato: `"usuario:comando;"`, para la comparación se usa la variable `nick`. Si el comando es solo un asterico (`*`) se excluiran todos los comando, ejemplo `root:*`; si utiliza un nombre con un asterisco se excluiran todos los comandos que "contengan" ese nombre, ejemplo: `user:drop*` (excluiría a dropbox entre otros); también se pueden definir varias exclusiones a la vez con el punto y coma (`;`), ejemplo  `user:drop*;user:wget;root:*`. |
| `-s`   | Separación vertical en pixeles entre usuarios, todos los procesos se agrupan por usuario para separar estos grupos se usa esta distancia. |
| `-n`   | Número de pixeles por caracter, usado para `${{offset x}}`. Cada caracter ocupa un espacio en pantalla, dependiendo del tipo de letra, para encolumnar números es útil definir cuanto espacio ocupan los números. |
| `-w`   | Ancho en caracteres de las columnas, usado para `${{offset x}}`. Para poder generar una tabla que no se mueva con los cambios de los numeros hay que definir adecadamente este parametro junto al anterior `-n`. |
| `-g`   | Activa la agrupación de los datos, para un mismo usuario y un mismo `nick`, se suman los datos numéricos. |

Las variables disponibles para usar en la plantilla son:

| Variable       | Descripción |
| :------------- | :---------- |
| `record_id`    | Identificador numerico único del proceso en cuestión. |
| `name`         | Comando completo del proceso que está usando la red. |
| `pid`          | ID del proceso en cuestión. |
| `uid`          | ID del usuario en cuestión. |
| `device_name ` | Nombre del dispositivo, eth0, wlp2s0, etc. |
| `sent_bytes`   | Bytes enviados por el comando. |
| `recv_bytes`   | Bytes recibios por el comando. |
| `sent_kbs`     | Velocidad en KiB/s enviados por el comando (promedio de los últimos 5 segundos). |
| `recv_kbs`     | Velocidad en KiB/s recibidos por el comando (promedio de los últimos 5 segundos). |
| `user`         | Nombre del usuario. |
| `nick`         | Nombre corto del comando. Si el comando es una secuencia de IPs, se muestra la IP que no sea la local, tratando de resolver por su nombre (hostname), ejemplo si name es `192.168.0.1:123-1.2.3.4:456` el nick será `1.2.3.4` si la dirección local es `192.168.0.1`; si el comando incluye la dirección completa del ejecutable con parámetros solo se considera el nombre del archivo, ejemplo si name es `/bin/wget URL -o /tmp/tmp` el nick será `wget`. |


## Notas

 * El comando `${{offset x}}`, que sirve para hacer tablas, es implementado en este 
   demonio, no corresponde a un comando de conky, la `x` es reemplazada por un número
   que se calcula para cada fila del archivo de salida (a base del texto que le precede
   y los parámetros `-n` y `-w`).
 * En este repositorio tiene un ejemplo de uso en conky, en el archivo `conkyrc-example`.
