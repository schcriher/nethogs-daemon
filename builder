#!/bin/bash
set -e

VER="1.0"
REV="2"
BASE="nethogs"
LIB="lib$BASE"
NAME="$BASE-daemon"
NETHOGS="https://github.com/raboof/$BASE"
DAEMON="https://raw.githubusercontent.com/schcriher/$NAME/master/$NAME.py"
URL="https://github.com/schcriher/$NAME"
ARCH="all"

if [ $(id -u) -ne 0 ];then
    echo "Debe ejecutarme con permisos de root." >&2
    exit 1
fi

echo "Creando el archivo deb..."

output="${PWD}/${NAME}_${VER}-${REV}_$ARCH.deb"
tmp="/tmp/$NAME"

rm -rf "$tmp"
mkdir -p "$tmp"
cd "$tmp"
mkdir -p "DEBIAN"

bin="/usr/bin"
app="/usr/share/$NAME"

exe="$bin/$NAME.py"
lib="$app/$LIB.so"
sts="/var/$NAME-status"

mkdir -p ".$bin"
mkdir -p ".$app"
mkdir -p "./var"

touch ".$exe"
touch ".$lib"
touch ".$sts"

cat > "DEBIAN/preinst" <<EOF
#!/bin/bash
set -e

killall $NAME ||:
rm -rf "$tmp"

mkdir -p "$bin"
mkdir -p "$app"
mkdir -p "$tmp"

cd "$tmp"

echo "Creando $LIB..."
git clone "$NETHOGS.git" .
old=\$(egrep -o "\s*uint32_t\s*sent_bytes\s*;" "src/$LIB.h" || :)
make $LIB
cp -f "src/$LIB.so."* "$lib"
cd -
rm -rf "$tmp"

echo "Descargando $NAME.py..."
wget $DAEMON -O "$exe"
sed -ir 's|LIB\s*=\s*".*libnethogs.so"\s*|LIB = "'"$lib"'"|' "$exe"
sed -ir 's|OUT\s*=\s*".*status"\s*|OUT = "'"$sts"'"|' "$exe"

if [ -n "\$old" ];then
    for var in sent recv;do
        sed -r "s|(.*"\$var"_bytes.*ctypes.c_uint)64(.*)|\132\2|" "$exe" > "$exe.fix"
        mv -f "$exe.fix" "$exe"
    done
fi

echo "Finalizando instalación..."
chmod +x "$exe"
touch "$sts"

exit 0
EOF

cat > "DEBIAN/prerm" <<EOF
#!/bin/bash
set -e

killall $NAME ||:

rm -rf "$exe"
rm -rf "$lib"
rm -rf "$sts"
rm -rf "$app"

rm -f "${exe}r"
rm -f "${exe}c"

exit 0
EOF

cat > "DEBIAN/control" <<EOF
Package: $NAME
Version: $VER-${REV}
Maintainer: $URL
Architecture: $ARCH
Section: net
Priority: optional
Homepage: $URL
Depends: libc6 (>= 2.15), libgcc1 (>= 1:3.0), libpcap0.8 (>= 0.9.8), libpcap0.8-dev, build-essential, libncurses5-dev
Recommends: conky, $BASE
Description: Daemon that saves in a file the complete state of nethogs
 Run $NAME.py in background as root and include in the configuration 
 file of conky \${execp cat $sts}
 .
 Library: $NETHOGS
 More info: $URL
EOF

chmod +x "DEBIAN/preinst"
chmod +x "DEBIAN/prerm"

dpkg --build . "$output"
rm -rf "$tmp"
