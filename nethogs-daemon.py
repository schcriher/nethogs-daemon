#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import re
import sys
import pwd
import ctypes
import signal
import socket
import argparse
import threading
import subprocess

LIB = "./libnethogs.so"
OUT = "./status"
TPL = "${{offset 12}}{o.nick:.20} ${{goto 140}}${{offset x}}{o.sent_kbs:.2f} ${{alignr}}{o.recv_kbs:.2f}   KiB/s"
EXC = ""
NOS = 7
WCC = 6
SEP = 5

LANG = os.getenv("LANGUAGE", "es")[:2].lower()

exclude = {}
records = {}

ips = re.compile(r'\w+(?:[.:-]\w+){9,17}')

class NethogsMonitorRecord(ctypes.Structure):
    _fields_ = (
        ('record_id', ctypes.c_int),
        ('name', ctypes.c_char_p),
        ('pid', ctypes.c_int),
        ('uid', ctypes.c_uint32),
        ('device_name', ctypes.c_char_p),
        ('sent_bytes', ctypes.c_uint64),
        ('recv_bytes', ctypes.c_uint64),
        ('sent_kbs', ctypes.c_float),
        ('recv_kbs', ctypes.c_float),
    )

class NethogsAppAction():
    SET = 1
    REMOVE = 2
    MAP = {SET: 'SET', REMOVE: 'REMOVE'}

class NethogsStatus():
    OK = 0
    FAILURE = 1
    NO_DEVICE = 2
    MAP = {OK: 'OK', FAILURE: 'FAILURE', NO_DEVICE: 'NO_DEVICE'}

def run_monitor(lib):
    struct_type = ctypes.POINTER(NethogsMonitorRecord)
    CALLBACK_TYPE = ctypes.CFUNCTYPE(ctypes.c_void_p, ctypes.c_int, struct_type)
    rc = lib.nethogsmonitor_loop(CALLBACK_TYPE(activity_callback))
    if rc != NethogsStatus.OK:
        print('nethogsmonitor_loop returned {}'.format(NethogsStatus.MAP[rc]))
    else:
        print('exiting monitor loop')

def get_nick(name):
    if name == "unknown TCP":
        return name
    elif ips.match(name):
        ipha, iphb = name.split("-")
        ipa = ipha.split(":")[0]
        ipb = iphb.split(":")[0]
        ip = ipb if ipa == localip else ipa
        try:
            return socket.gethostbyaddr(ip)[0]
        except:
            return ip
    else:
        return name.split(" ", 1)[0].split("/")[-1]

def show(data):
    for cmd in exclude.get(data.user, []):
        if cmd is True:
            return False
        if "*" in cmd:
            cond = cmd.replace("*", "") in data.nick
        else:
            cond = cmd == data.nick
        if cond:
            return False
    return True

def group_names(process):
    _process = {}
    for pid, data in process.items():
        for _pid, _data in _process.items():
            if data.nick == _data.nick:
                _data.sent_bytes += data.sent_bytes
                _data.recv_bytes += data.recv_bytes
                _data.sent_kbs += data.sent_kbs
                _data.recv_kbs += data.recv_kbs
                break
        else:
            _process[pid] = data
    return _process

def activity_callback(action, record):
    data = record.contents
    data.user = pwd.getpwuid(data.uid).pw_name
    data.nick = get_nick(data.name.decode())

    if action == NethogsAppAction.SET:
        if show(data):
            records.setdefault(data.user, {})[data.pid] = data
    else:
        records.setdefault(data.user, {}).pop(data.pid, None)

    lines = []
    len_list = []
    row_list = []
    for i in range(n_offset_x):
        len_list.append([])
        row_list.append([])

    first_user = True
    for user in sorted(records):
        process = records[user]
        if process:
            if first_user:
                lines.append(user)
                first_user = False
            else:
                lines.append("${{voffset {}}}{}".format(args.s, user))
            if args.g:
                process = group_names(process)
            for pid in sorted(process):
                line = args.t.format(o=process[pid])
                if n_offset_x:
                    for col, item in enumerate(line.split("${offset x}")[1:]):
                        n = len(item.lstrip().split(" ", 1)[0])
                        len_list[col].append(n)
                        row_list[col].append(len(lines))
                lines.append(line)

    if n_offset_x:
        for lens, rows in zip(len_list, row_list):
            if len(lens) > 0:
                m = max(lens)
                p = args.w - m if args.w > m else 0
                for l, r in zip(lens, rows):
                    fix = (m - l + p) * args.n
                    lines[r] = lines[r].replace("${offset x}", "${{offset {}}}".format(fix), 1)

    code = "\n".join(lines)
    with open(args.o, encoding='utf-8', mode='w') as fid:
        fid.write(code)

def signal_handler(signal, frame):
    lib.nethogsmonitor_breakloop()

# Ideally, use gettext, but a single file is preferred.
if LANG == "es":
    l = """Localización de la librería `libnethogs.so`, la dirección absoluta 
o relativa al demonio, o solo el nombre exacto del archivo y Python la 
buscará entre las librerías del sistema."""
    o = """Localización del archivo de salida, por ejemplo para ser incluido 
en conky."""
    t = """Plantilla del archivo de salida, los comandos en conky usan llaves, 
estas deben introducirse duplicadas, ejemplo `${{goto 0}}`, para que 
Python no las interprete como variables. Los datos disponibles en el 
demonio se acceden con la variable `o`, ejemplo `{o.recv_kbs}`; puede 
usar el formateo de cadenas de Python, ejemplo `{o.recv_kbs:.2f}` 
para mostrar dos decimales."""
    e = """Lista de comandos a excluir por usuario, con el formato: 
`"usuario:comando;"`, para la comparación se usa la variable `nick`. 
Si el comando es solo un asterico (`*`) se excluiran todos los comando, 
ejemplo `root:*`; si utiliza un nombre con un asterisco se excluiran 
todos los comandos que "contengan" ese nombre, ejemplo: `user:drop*` 
(excluiría a dropbox entre otros); también se pueden definir varias 
exclusiones a la vez con el punto y coma (`;`), ejemplo  
`user:drop*;user:wget;root:*`."""
    s = """Separación vertical en pixeles entre usuarios, todos los procesos 
se agrupan por usuario para separar estos grupos se usa esta distancia."""
    n = """Número de pixeles por caracter, usado para `${{offset x}}`. Cada 
caracter ocupa un espacio en pantalla, dependiendo del tipo de letra, 
para encolumnar números es útil definir cuanto espacio ocupan los números."""
    w = """Ancho en caracteres de las columnas, usado para `${{offset x}}`. 
Para poder generar una tabla que no se mueva con los cambios de los 
numeros hay que definir adecadamente este parametro junto al anterior 
`-n`."""
    g = """Activa la agrupación de los datos, para un mismo usuario y un mismo 
`nick`, se suman los datos numéricos."""
    h = """Muestra esta ayuda y notas útiles."""
    info = """
Para incluir la salida de este script en conky use: ${{execp cat /path/to/status-file}}
El 'status-file' es el fijado por el parámetro `-o`, por defecto: '{}'

En la plantilla (parámetros `-t`) puede usar `${{{{offset x}}}}` donde la `x` será 
reemplazada por un número que se calcula a base de los parámetros `-n` y `-w` 
y la longitud en caracteres de lo que continue a este comando. Puede usar todo 
el sistema de formateo de Python, dispone de mas información en: 
https://docs.python.org/3/library/string.html#format-string-syntax

Plantilla por defecto: {}
"""
    root = """Debe ejecutarme con privilegios de root."""

else:
    l = """Location of the library `libnethogs.so`, absolute or relative to 
the daemon, or just the exact name of the file and Python will 
look for it among the libraries of the system."""
    o = """Location of the output file, for example to be included in conky."""
    t = """Output file template. The commands in conky use keys, these must 
be introduced duplicates, example `${{goto 0}}`, so that Python 
does not interpret them as variables. The data available in the 
daemon is accessed with the variable `o`, example `{o.recv_kbs}`; 
you can use Python string formatting, example `{o.recv_kbs:.2f}` 
to display two decimal places."""
    e = """List of commands to be excluded per user, with the format: 
`'user:command;'`, the `nick` variable is used for the comparison. 
If the command is only an asterisk (`*`) all the commands are 
excluded, eg `root:*`; if you use a name with an asterisk, all 
the commands that contain this name will be excluded, for example: 
`user:drop*` (exclude dropbox among others); you can also define 
several exclusions at once with the semicolon (`;`), example 
`user:drop*;user:wget;root:*`."""
    s = """Vertical separation in pixels between users, all processes are 
grouped by user to separate these groups this distance is used."""
    n = """Number of pixels per character, used for `${{offset x}}`. Each 
character occupies a space on the screen, depending on the type 
of letter, to column numbers, it is useful to define how much 
space numbers occupy."""
    w = """Width in characters of the columns, used for `${{offset x}}`. 
To be able to generate a table that does not move with the 
changes of the numbers, it is necessary to define this 
parameter in addition to the previous `-n`."""
    g = """It activates the grouping of the data, for the same user and 
a same `nick`, the numerical data are added."""
    h = """Show this help and useful notes."""
    info = """
To include the output of this script in conky use: ${{execp cat /path/to/status-file}}
The 'status-file' is set by the `-o` parameter, by default: '{}'

In the template (`-t` parameters) you can use `${{{{offset x}}}}` where the `x` 
will be replaced by a number that is calculated based on the `-n` and `-w` 
parameters and the length in characters of what continues to this command. 
You can use the entire Python formatting system, you have more information 
on: https://docs.python.org/3/library/string.html#format-string-syntax

Default template: {}
"""
    root = """Must run with root privileges."""

if __name__ == '__main__':
    proc = subprocess.Popen(["hostname", "-I"], stdout=subprocess.PIPE)
    localip = proc.communicate()[0].decode().strip()

    arguments = (
        ("-l", "store",      LIB,   str),
        ("-o", "store",      OUT,   str),
        ("-t", "store",      TPL,   str),
        ("-e", "store",      EXC,   str),
        ("-s", "store",      SEP,   int),
        ("-n", "store",      NOS,   int),
        ("-w", "store",      WCC,   int),
        ("-g", "store_true", False, None),
        ("-h", "store_true", False, None),
    )
    parser = argparse.ArgumentParser(add_help=False)
    loc = locals()
    for arg, action, default, type in arguments:
        cfg = {
            "help": loc[arg[1]],
            "action": action,
            "default": default,
        }
        if type == "store":
            cfg["type"] = type
        parser.add_argument(arg, **cfg)
    args = parser.parse_args()

    args.o = os.path.abspath(args.o)

    if args.h:
        parser.print_help()
        print(info.format(args.o, args.t))
        sys.exit(0)

    if os.geteuid() != 0:
        print(root)
        sys.exit(1)

    args.t = args.t.replace("${offset x}", "${{offset x}}")
    n_offset_x = args.t.count("${{offset x}}")

    for rule in args.e.split(";"):
        if ":" in rule:
            usr, cmd = rule.split(":")
            if cmd == "*":
                exclude.setdefault(usr, []).append(True)
            else:
                exclude.setdefault(usr, []).append(cmd)

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    lib = ctypes.CDLL(args.l)
    monitor_thread = threading.Thread(target=run_monitor, args=(lib,))
    monitor_thread.start()

    done = False
    while not done:
        monitor_thread.join(0.3)
        done = not monitor_thread.is_alive()
